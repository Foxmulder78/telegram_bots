from telegram.ext import Updater
from telegram.ext import CommandHandler

# ------ Macau facts

def get_fact(n):
    import re

    infile = 'macau_facts.txt'
    exp = r'^(%d\)\s)(.*)' % n

    with open(infile) as f:
        res = re.search(exp,f.read(),re.MULTILINE)
        if res != []:
            return res.group(2)

    return 'No results'

def facts(bot, update):
    from random import randint

    n = randint(1,45)
    fact = get_fact(n)
    bot.sendMessage(chat_id=update.message.chat_id, text=fact)

# ------ Wolfram

def wolfram_query(query):
    import wolframalpha

    #query = 'temperature in Macau on September 17, 1992'

    appid='QURL3P-V8YWK4P9T4'
    client = wolframalpha.Client(appid)
    res = client.query(query)
    reply = next(res.results).text
    print reply

    return reply

def alpha(bot, update, args):
    query = ' '.join(args)
    print query
    reply = wolfram_query(query)
    print reply
    bot.sendMessage(chat_id=update.message.chat_id, text=reply)

# ------ Take screenshot of a page

def screenshot(url = 'http://www.google.com'):
    from subprocess import call
    import requests
    
    # Run java script that gets the screenshot and saves to local directory as 'screenshot.png'
    cmd = ['phantomjs','screenshot.js',url]
    call(cmd)
    
# ------ Upload file to telegram server

def upload_photo(file_ = 'screenshot.pnd'):

    url_telegram = 'https://api.telegram.org/bot' + token +'/sendPhoto'
    print('upload url: %s' % url_telegram)
    file_ = {'photo':open('screenshot.png', 'rb')}
    data = {'chat_id': chat_id}
    r = requests.post(url_telegram, data = data, files=file_)
    print('requests: %s' % r)

# ------ Send photo

def img(bot, update, args):
    url = ' '.join(args)
    print('url: %s' % url)
    screenshot(url)
    bot.sendPhoto(chat_id=update.message.chat_id, photo='screenshot.png')

chat_id = '226814852'
token = chat_id + ':AAHCYFR3fjGBLJdwJ4mR9SGs9t7_J66bxUU'
updater = Updater(token=token)
dispatcher = updater.dispatcher

wolfram_handler = CommandHandler('alpha', alpha, pass_args=True)
dispatcher.add_handler(wolfram_handler)

img_handler = CommandHandler('img', img, pass_args=True)
dispatcher.add_handler(img_handler)

facts_handler = CommandHandler('facts', facts, pass_args=False)
dispatcher.add_handler(facts_handler)

#start the bot
updater.start_polling()
updater.idle()
